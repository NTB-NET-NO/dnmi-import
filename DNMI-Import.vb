Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib.LogFile


Public Class DNMI_ImportService
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New DNMI_ImportService}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'DNMI_ImportService
        '
        Me.ServiceName = "NTB_DNMI-Import"

    End Sub

#End Region

    Protected workObj As RTFImportObject

    'Service started
    Protected Overrides Sub OnStart(ByVal args() As String)

        workObj = New RTFImportObject
        workObj.Initiate()

        Dim log As String = AppSettings("logFolder")
        WriteLog(log, "DNMI Import started.")
    End Sub

    'Service stopped
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        workObj.StopEvents()

        Dim log As String = AppSettings("logFolder")
        WriteLog(log, "DNMI Import stopped.")
    End Sub

    'Server shutdown
    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub

End Class
