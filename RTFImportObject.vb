Imports System.IO
Imports System.text
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports ntb_FuncLib

Imports Word

Public Class RTFImportObject
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        wordAppObject = New Application

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()

        wordAppObject.Quit()
        wordAppObject = Nothing
    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FileWatcher As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileWatcher = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 5000
        CType(Me.FileWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Protected wordAppObject As ApplicationClass
    Protected wordMal As String

    Protected inputFolder As String
    Protected outputFolder As String
    Protected tempFolder As String
    Protected logFolder As String
    Protected doneFolder As String
    Protected errorFolder As String

    Protected deleteTemp As Boolean

    Protected headers As Hashtable = New Hashtable

    Public Sub Initiate()
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        logFolder = AppSettings("logFolder")
        If Not Directory.Exists(logFolder) Then Directory.CreateDirectory(logFolder)

        LogFile.WriteLog(logFolder, "DNMI Import service initiating...")

        inputFolder = AppSettings("inputFolder")
        outputFolder = AppSettings("outputFolder")
        tempFolder = AppSettings("tempFolder")
        doneFolder = AppSettings("doneFolder")
        errorFolder = AppSettings("errorFolder")

        If Not Directory.Exists(inputFolder) Then Directory.CreateDirectory(inputFolder)
        If Not Directory.Exists(outputFolder) Then Directory.CreateDirectory(outputFolder)
        If Not Directory.Exists(tempFolder) Then Directory.CreateDirectory(tempFolder)
        If Not Directory.Exists(doneFolder) Then Directory.CreateDirectory(doneFolder)
        If Not Directory.Exists(errorFolder) Then Directory.CreateDirectory(errorFolder)

        wordMal = AppSettings("wordMal")
        deleteTemp = AppSettings("deleteTempFiles")

        Try
            Dim conf As XmlDocument = New XmlDocument
            conf.Load(AppSettings("configFile"))

            Dim nd As Xml.XmlNode
            Dim ndlist As XmlNodeList = conf.SelectNodes("/headers/header")

            For Each nd In ndlist
                Dim inn As StreamReader = New StreamReader(nd.Attributes("headerFile").Value, Encoding.GetEncoding("iso-8859-1"))
                headers(nd.Attributes("filter").Value) = inn.ReadToEnd
                inn.Close()

                LogFile.WriteLog(logFolder, "Filter: " & nd.Attributes("filter").Value & " -> " & nd.Attributes("headerFile").Value)
            Next

        Catch ex As Exception
            LogFile.WriteErr(logFolder, "Error reading config files", ex)
        End Try

        Try
            FileWatcher.Path = inputFolder
            PollTimer.Start()
        Catch ex As Exception
            LogFile.WriteErr(logFolder, "Error starting", ex)
        End Try
    End Sub

    'Stop events
    Public Sub StopEvents()
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False
    End Sub

    'Timed polling
    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        DoAllFiles()

        PollTimer.Interval = AppSettings("pollingInterval") * 1000
        PollTimer.Start()
        FileWatcher.EnableRaisingEvents = True
    End Sub

    'Event polling
    Private Sub FileWatcher_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileWatcher.Created
        PollTimer.Stop()
        FileWatcher.EnableRaisingEvents = False

        DoAllFiles()

        PollTimer.Interval = AppSettings("pollingInterval") * 1000
        PollTimer.Start()
        FileWatcher.EnableRaisingEvents = True
    End Sub

    'Loops and handles all the files in the input folder
    Protected Sub DoAllFiles()

        Dim filter As String
        For Each filter In headers.Keys

            'Get files
            Dim files As String() = Directory.GetFiles(inputFolder, filter)

            'Wait.... wait... wait.... 
            System.Threading.Thread.Sleep(3000)

            'Go...
            Dim f As String
            For Each f In files
                DoOneFile(f, headers(filter))
            Next

        Next

    End Sub

    'Handles one input file
    Protected Sub DoOneFile(ByVal filename As String, ByVal header As String)

        Dim tmp As String = ConvertTxtToRTF(filename)
        Dim id As String = "RED" & Now.ToString("yyMMdd") & "_" & Now.ToString("HHmmssf") & "_dnmi"

        If tmp <> "" Then

            Try
                Dim inn As StreamReader = New StreamReader(tmp, Encoding.GetEncoding("iso-8859-1"))
                Dim ut As StreamWriter = New StreamWriter(outputFolder & "\" & Path.GetFileNameWithoutExtension(filename) & ".rtf", False, Encoding.GetEncoding("iso-8859-1"))

                ut.Write(header.Replace("<ID>", id))
                ut.Write(inn.ReadToEnd)

                inn.Close()
                ut.Close()

                LogFile.WriteLog(logFolder, "RTF with FIP-header written: " & outputFolder & "\" & Path.GetFileNameWithoutExtension(filename) & ".rtf")
                File.Copy(filename, FuncLib.MakeSubDirDate(doneFolder & "\" & Path.GetFileName(filename), File.GetLastWriteTime(filename)), True)

            Catch ex As Exception
                'Log error
                LogFile.WriteErr(logFolder, "Error writing file: " & outputFolder & "\" & Path.GetFileNameWithoutExtension(filename) & ".rtf", ex)
                File.Copy(filename, FuncLib.MakeSubDirDate(errorFolder & "\" & Path.GetFileName(filename), File.GetLastWriteTime(filename)), True)
            End Try

            'Cleanup
            Try

                File.Delete(filename)
                If deleteTemp Then
                    File.Delete(tmp)
                End If

            Catch ex As Exception
                'Log error
                LogFile.WriteErr(logFolder, "Cleanup failed", ex)
            End Try

        End If

    End Sub

    'Main conversion
    Protected Function ConvertTxtToRTF(ByVal filename As String) As String

        'Document
        Dim tmpFile As String = tempFolder & "\" & Path.GetFileNameWithoutExtension(filename) & ".rtf"
        Dim wdDoc1, wdDoc2 As DocumentClass

        Try
            wdDoc1 = wordAppObject.Documents.Open(filename, _
                ConfirmConversions:=False, _
                ReadOnly:=True, _
                AddToRecentFiles:=False, _
                PasswordDocument:="", _
                PasswordTemplate:="", _
                Revert:=False, _
                WritePasswordDocument:="", _
                WritePasswordTemplate:="", _
                Format:=Word.WdOpenFormat.wdOpenFormatAuto, _
                Visible:=False)

            If wordMal <> "" Then
                wdDoc2 = wordAppObject.Documents.Add(Template:=wordMal)
            Else
                wdDoc2 = wordAppObject.Documents.Add()
            End If

            'Loop paragraphs and set styles her.
            'wdDoc.Paragraphs.First.Style = "_InfoLinje"

            Dim trimChars As Char() = {" ", vbCr, vbLf}
            Dim txt As String
            Dim p As Paragraph
            Dim first As Boolean = True

            Dim i As Integer
            For i = 1 To wdDoc1.Content.Paragraphs.Count

                txt = wdDoc1.Content.Paragraphs(i).Range.Text.Trim(trimChars)

                If first Then
                    p = wdDoc2.Paragraphs(1)
                    p.Range.Text = txt

                    Try
                        p.Style = wdDoc2.Styles("_Infolinje")
                    Catch ex As System.Runtime.InteropServices.COMException
                        LogFile.WriteErr(logFolder, "Error setting style ( Error: " & ex.ErrorCode & " )", ex)
                    End Try

                    p.Range.InsertParagraphAfter()
                    first = False
                Else
                    p = wdDoc2.Content.Paragraphs.Add()
                    p.Range.Text = txt

                    Try
                        p.Style = wdDoc2.Styles("_Br�dtekst")
                    Catch ex As System.Runtime.InteropServices.COMException
                        LogFile.WriteErr(logFolder, "Error setting style ( Error: " & ex.ErrorCode & " )", ex)
                    End Try

                    p.Range.InsertParagraphAfter()
                End If

            Next

            wdDoc2.SaveAs(tmpFile, Word.WdSaveFormat.wdFormatRTF)

        Catch e As System.Runtime.InteropServices.COMException

            LogFile.WriteErr(logFolder, "Error converting to RTF with Word2003: " & filename & "( Error: " & e.ErrorCode & " )", e)

            wdDoc1.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
            wdDoc2.Close(Word.WdSaveOptions.wdDoNotSaveChanges)

            Return ""
        End Try

        wdDoc1.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
        wdDoc2.Close(Word.WdSaveOptions.wdDoNotSaveChanges)

        LogFile.WriteLog(logFolder, "File converted to RTF via Word2003: " & filename)

        Return tmpFile

    End Function

End Class
